const mongoose = require('mongoose');
const mongooseConnection = mongoose.connect('mongodb://localhost/test');

export default mongooseConnection;
