import express from 'express';
import bodyParser from 'body-parser';


import { port } from './config/app';
import { createApi } from './modules/todo';

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

createApi(app, '/api/todo');

console.log(`Listening on port: ${port}`);
app.listen(port, () => undefined);