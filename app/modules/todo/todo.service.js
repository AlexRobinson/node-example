import Todo from './todo.model';

export function createTodo(title, callback) {
    const todo = new Todo({
        title
    });
    todo.save(callback);
}

export function getAllTodos(callback) {
    Todo.find(callback)
}

export function getTodo(id, callback) {
    Todo.findById(id, callback);
}

export function deleteTodo(id, callback) {
    Todo.findByIdAndRemove(id, callback);
}