import { getAllTodos, getTodo, createTodo, deleteTodo } from './todo.service';

export default function (app, prefix) {
    app.get(`${prefix}`, (req, res) => {
        getAllTodos((err, todos) => {
            if (err) {
                res.send(500, err);
            }
            res.json(todos);
        });
    });

    app.get(`${prefix}/:id`, (req, res) => {
        getTodo(req.params.id, (err, todo) => {
            if (err) {
                res.send(500, err);
            }
            res.json(todo);
        });
    });

    app.post(`${prefix}`, (req, res) => {
        console.log('creating todo', req.body);
        createTodo(req.body.title, (err, todo) => {
            if(err) {
                res.send(500, err);
            }
            res.json(201, todo);
        })
    });

    app.delete(`${prefix}/:id`, (req, res) => {
        deleteTodo(req.params.id, (err) => {
            if(err) {
                res.send(500, err);
            }
            res.send(204);
        });
    });
}