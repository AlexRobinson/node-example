import todoModel from './todo.model';
import todoService from './todo.service';
import todoApi from './todo.api';

export const Todo = todoModel;
export const service = todoService;
export const createApi = todoApi;
