import mongoose from '../../config/mongoose';

const Todo = mongoose.model('Todo', {
    title: {type: String, required: true}
});

export default Todo;